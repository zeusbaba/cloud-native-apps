Cloud-Native Apps  
===================  

Language-agnostic microservices developed as cloud-native apps  
_Learning, Making, Sharing..!_  

# How LEARN & MAKE  
Each app/codebase contains its own _README_ for easy onboarding.   

I'll begin with re-implementing BAET.no's API Server by leveraging different programming languages, and make them as cloud-native apps! 

See list:  
- [Web/Frontend using Node.js/Vue.js](baet-web-vuejs): crafted with Vue.js, Docker, docker-compose, k8s  
- [API Server using Node.js/FeathersJS](baet-api-js): crafted with FeathersJS, Docker, docker-compose, k8s  
- [API Server using Golang/mux](baet-api-golang): crafting with Golang, mux, Docker, docker-compose, k8s  


# How to SHOW & SHARE     
[BAET.no](https://baet.no) is full-functional link shortener service, and is running based on this codebase.   
API is accessible using this [Postman collection](https://documenter.getpostman.com/view/2611563/RzfZPt3c)  
